
## Web Server Log Parser

App contains an artisan command for parse log file that work 
with batching jobs .And a route for count of filtered logs.

### Running

1.running containers

```shell
docker-compose up --build
```

2.running migrations

```shell
./vendor/bin/sail artisan migrate
```

3.running log parser
```shell
./vendor/bin/sail artisan log:reader:start --chunk=50
```

### test

```shell
./vendor/bin/sail artisan test
```
