<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {

	public function up ()
	{
		Schema ::create('logs', function (Blueprint $table) {
			$table -> id();
            $table->string('service-name',50);
            $table->integer('status-code');
            $table->string('url',255);
            $table->timestamp('date-time');
            $table->string('method' ,10);
            $table->string('request',10);
			$table -> timestamps();
		});
	}

	public function down ()
	{
		Schema ::dropIfExists('logs');
	}
};
