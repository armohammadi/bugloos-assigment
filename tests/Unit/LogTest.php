<?php

namespace Tests\Unit;


use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase;
use Tests\CreatesApplication;

class LogTest extends TestCase
{

    use CreatesApplication, DatabaseMigrations;

    /**
     * A basic test example.
     */
    public function test_log_reader_command_search (): void
    {
        $this -> artisan('log:reader:start')
            -> expectsQuestion('Where is path of log file? ( from __DIR__/storage/app )', 'test/logs.txt')
            -> assertSuccessful();

        $this -> assertDatabaseHas('logs',
            [
                "id"           => 1,
                "service-name" => "order-service",
                "status-code"  => 201,
                "url"          => "/orders",
                "date-time"    => "2022-09-17 10:21:53",
                "method"       => "POST",
                "request"      => "HTTP/1.1",
            ]);

        $this->assertDatabaseCount('logs',200);

    }

}
