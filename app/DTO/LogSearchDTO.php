<?php

namespace App\DTO;

class LogSearchDTO
{

    public function __construct (
        private ?string $serviceName = NULL,
        private ?string $statusCode = NULL,
        private         $startDate = NULL,
        private         $endDate = NULL,
    )
    {
    }

    /**
     * @return string
     */
    public function getServiceName (): string
    {
        return $this -> serviceName;
    }

    /**
     * @param string $serviceName
     *
     * @return LogSearchDTO
     */
    public function setServiceName (string $serviceName): LogSearchDTO
    {
        $this -> serviceName = $serviceName;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatusCode (): string
    {
        return $this -> statusCode;
    }

    /**
     * @param string $statusCode
     *
     * @return LogSearchDTO
     */
    public function setStatusCode (string $statusCode): LogSearchDTO
    {
        $this -> statusCode = $statusCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStartDate ()
    {
        return $this -> startDate;
    }

    /**
     * @param mixed $startDate
     *
     * @return LogSearchDTO
     */
    public function setStartDate ($startDate)
    {
        $this -> startDate = $startDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndDate ()
    {
        return $this -> endDate;
    }

    /**
     * @param mixed $endDate
     *
     * @return LogSearchDTO
     */
    public function setEndDate ($endDate)
    {
        $this -> endDate = $endDate;

        return $this;
    }

    /**
     * @param array $data
     *
     * @return self[]
     */
    public static function createFromArray (array $data)
    {
        $logSearchDTOs = [];
        foreach ($data as $d)
        {
            $logSearchDTOs[] = (new LogSearchDTO())
                ->setServiceName($d['serviceName'] ?? '')
                ->setStatusCode($d['statusCode'] ?? '')
                ->setStartDate($d['startDate'] ?? NULL)
                ->setEndDate($d['endDate'] ?? NULL);
        }

        return $logSearchDTOs;
    }

}
