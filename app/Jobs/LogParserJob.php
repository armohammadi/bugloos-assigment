<?php

namespace App\Jobs;

use App\Models\Log;
use App\Services\LogParserService;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;

class LogParserJob implements ShouldQueue
{

    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private LogParserService $parser;
	public function __construct (
        private $logLines,
        private $format = '%{Service}i - %t "%m %U %r" %>s'.PHP_EOL
    )
	{
        $this->parser = app()->makeWith(LogParserService::class,['format' => $this->format]);
	}

	public function handle ()
	{
        foreach ($this->logLines as $log)
        {
            if ($this->filterLines($log))
            {
                $logModelArray[] = $this -> createLogModel($this -> parser -> parse($log));
            }
        }

       return Log::query()->insert($logModelArray?? []);
	}

    private function createLogModel ($data)
    {
        return [
               'service-name' => $data->HeaderService ,
               'status-code' => $data->status,
               'url' => $data->URL,
               'date-time' => Carbon::createFromFormat('d/M/Y:h:i:s',$data->time),
               'method' => $data->requestMethod,
               'request' => $data->request,
               'created_at' => now(),
               'updated_at' => now()
            ];
    }

    private function filterLines ($line)
    {
        return ($line && $line != PHP_EOL);
    }
}
