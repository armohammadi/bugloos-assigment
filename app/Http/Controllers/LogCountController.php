<?php

namespace App\Http\Controllers;

use App\DTO\LogSearchDTO;
use App\Http\Requests\LogCountRequest;
use App\Models\Log;
use Illuminate\Http\Response;

class LogCountController extends Controller
{

    /**
     * @param \App\Http\Requests\LogCountRequest $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Foundation\Application|\Illuminate\Http\Response
     */
    public function __invoke (LogCountRequest $request)
    {

        return response([
            "count" => Log ::filter(LogSearchDTO ::createFromArray($request -> input('filters'))) -> count()
        ]);
    }
}
