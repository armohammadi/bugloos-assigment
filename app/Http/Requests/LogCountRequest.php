<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LogCountRequest extends FormRequest
{

    public function rules (): array
    {
        return [
            'filters'                => ['array'],
            'filters.serviceNames'   => ['filled', 'array'],
            'filters.serviceNames.*' => ['string', 'filled'],
            'filters.statusCode'     => ['filled', 'numeric'],
            'filters.startDate'      => ['required_with:endDate', 'date'],
            'filters.endDate'        => ['required_with:startDate', 'date'],
        ];
    }
}
