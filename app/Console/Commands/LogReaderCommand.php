<?php

namespace App\Console\Commands;

use App\Jobs\LogParserJob;
use Illuminate\Console\Command;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Storage;


class LogReaderCommand extends Command
{

	protected $signature = 'log:reader:start {chunk=50}';

    protected $defaultPath = 'public/logs.txt';

	protected $description = 'Read web server logs and store it to db with chunk and use batching job';

    /**
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle ()
	{
		$logPath = $this->ask('Where is path of log file? ( from __DIR__/storage/app )',$this->defaultPath);

        $this -> fileChecker($logPath);

        $batch = $this -> fileReader($logPath);

        $this -> processMaker($batch, $logPath);
    }

    /**
     * @param mixed $logPath
     *
     * @return int|void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function fileChecker (mixed $logPath)
    {
        if (!Storage ::disk('local') -> exists($logPath))
        {
            $this -> error('file not found.');

            throw new FileNotFoundException();
        }

        $this->info('file founded!');
    }

    /**
     * @param array $batch
     * @param mixed $logPath
     *
     * @return void
     */
    public function processMaker (array $batch, mixed $logPath): void
    {
        $bus = Bus ::batch($batch)
            -> name("log_inserter_{$logPath}_" . Carbon ::now())
            ->dispatch();

        $this->info("process with id:{$bus->id} started.");
    }

    /**
     * @param bool $file
     *
     * @return array
     */
    public function fileReader (string $logPath): array
    {

        $this->info('reading started.');

        $file = fopen(storage_path("app/$logPath"), 'rb');

        $bar = $this->output->createProgressBar($this->getLines($logPath));

        $bar->start();

        if ($file)
        {
            while (!feof($file))
            {
                $logLines[] = fgets($file);
                if (feof($file) || count($logLines) >= $this -> argument('chunk'))
                {

                    $batch[] = new LogParserJob($logLines);

                    $bar->advance(count($logLines));

                    $logLines = [];
                }
            }


        }

        $bar->finish();

        $this->info('reading finished.');

        return $batch ?? [];
    }

    public function getLines($logPath)
    {
        $file = fopen(storage_path("app/$logPath"), 'rb');
        $lines = 0;
        while(!feof($file)) {
            $lines += substr_count(fread($file, 8192), "\n");
        }
        fclose($file);

        return $lines;
    }
}
