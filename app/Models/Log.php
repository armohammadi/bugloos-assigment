<?php

namespace App\Models;

use App\DTO\LogSearchDTO;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static filter(LogSearchDTO[] $searchData): Builder
 */
class Log extends Model
{

    protected $fillable =
        [
            'service-name',
            'status-code',
            'url',
            'date-time',
            'method',
            'request',
        ];


    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param LogSearchDTO[]                        $searchData
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilter (Builder $query, array $searchData): Builder
    {
        foreach ($searchData as $data)
        {
            $query -> orWhere(function (Builder $query) use ($data) {
                $query -> search($data);
            });
        }

        return $query;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \App\DTO\LogSearchDTO                 $searchData
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearch (Builder $query, LogSearchDTO $searchData): Builder
    {
        return $query -> serviceName($searchData -> getServiceName())
            -> statusCode($searchData -> getStatusCode())
            -> date(
                $searchData -> getStartDate(),
                $searchData -> getEndDate()
            );
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $serviceName
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeServiceName (Builder $query, $serviceName): Builder
    {
        return $query -> where('service-name', $serviceName);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $statusCode
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeStatusCode (Builder $query, $statusCode): Builder
    {
        return $query -> where('status-code', $statusCode);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $startDate
     * @param                                       $endDate
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDate (Builder $query, $startDate, $endDate): Builder
    {
        return $query -> whereBetween('date-time', $startDate, $endDate);
    }
}
